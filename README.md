# mr

Taken from here:
https://github.com/helpermethod/mr
and updated slightly to work with WSL.

Need to have chrome, or the browser of your choice available on your path

## Installation
Add the mr script to your path

## Use
Run mr on a checked out branch, and it will open a new chrome page with the 
delete branch and squash commits fields ticked.